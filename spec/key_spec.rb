require "rails_helper"

def create_set
  $redis.flushdb
  2.times { |x| $redis.sadd 'primal_test_set', x }
end

RSpec.describe KeyController, type: :controller do

  it "takes one code from set" do
    create_set

    get :get_key
    expect(response.body == '<strong>No codes for you. Sorry</strong>' || response.body.blank?).to eq false
  end

  it "checks uniq of responses" do
    create_set
    codes = []

    2.times do |i|
        get :get_key
        codes << response.body
    end

    expect(codes.uniq).to eq codes
  end


  it "makes set empty" do
    create_set

    3.times { get :get_key }
    expect(response.body).to eq "<strong>No codes for you. Sorry</strong>"
  end


end

