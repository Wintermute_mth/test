class KeyController < ApplicationController

  def get_key
    $redis.watch PRIMAL_SET_NAME
    code = $redis.multi { $redis.spop PRIMAL_SET_NAME } .first
    $redis.sadd 'used_set', code

    if code
      render json: code
    else
      render html: "<strong>No codes for you. Sorry</strong>".html_safe
    end
  end

end
