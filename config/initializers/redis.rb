$redis = Redis.new(:host => 'localhost', :port => 6379)


if Rails.env == "test"
  PRIMAL_SET_NAME = "primal_test_set"
else
  PRIMAL_SET_NAME = "primal_set"
end

3.times { |x| $redis.sadd PRIMAL_SET_NAME, x }

#3.times { $redis.sadd 'primal_set', SecureRandom.hex }
